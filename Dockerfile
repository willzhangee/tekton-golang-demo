FROM golang:1.21.5-alpine as builder

ENV GOPROXY https://goproxy.cn

# Create and change to the app directory.
WORKDIR /app

# Retrieve application dependencies.
# This allows the container build to reuse cached dependencies.
# Expecting to copy go.mod and if present go.sum.
COPY . .
RUN go mod download

# Build the binary.
RUN go build -v -o app-server

FROM alpine
RUN sed -i 's/dl-cdn.alpinelinux.org/mirrors.ustc.edu.cn/g' /etc/apk/repositories && \
    apk add -U tzdata
RUN ln -sf /usr/share/zoneinfo/Asia/Shanghai /etc/localtime
COPY --from=builder /app/app-server /usr/local/bin/app-server
EXPOSE 8080
CMD [ "app-server" ]
